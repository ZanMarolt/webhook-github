const express        = require('express');
const bodyParser     = require('body-parser');
const cors           = require('cors');
const { execSync }   = require('child_process');

// use in your express app
const app = express();

const PORT = 3366

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.listen(PORT, () => { console.log('Server running on port: ' + PORT)});

app.post('/webhook/burger-app', function (req, res) {
  console.log(req.body);
// stderr is sent to stderr of parent process
// you can set options.stdio if you want it to go elsewhere
const { exec } = require('child_process');
exec('ls -al', (err, stdout, stderr) => {
  if (err) {
    // node couldn't execute the command
    return;
  }

  // the *entire* stdout and stderr (buffered)
  console.log(`stdout: ${stdout}`);
});
  res.send('Got a POST request')
})
